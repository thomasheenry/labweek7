﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using LabWeek7.Models;
using LabWeek7.Utils;

namespace LabWeek7.ViewModels;

public class MainWindowViewModel(TopLevel? topLevel) : ViewModelBase
{
    private readonly FileManagement? _fileManager = new(topLevel);
    public static ObservableCollection<User> Users { get; set;} = [];
    public static ObservableCollection<User> RestoreUsers { get; set;} = [];


    public  void AddNewUser(User user)
    {
        Users.Add(user);
        
        ObserverMessage messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"User {user.Email} has been added.");
      
        RestoreUsers.Add(user);
    }
    
    public  void FilterUser(string email) {
        string emailRemoveSpaces = email.Trim();

        if (email == "") RestoreUsersFilter();

        List<User> UsersFiltered = Users.Where(obj => obj.Email.Contains(emailRemoveSpaces)).ToList();
        
        FilteredUsers(UsersFiltered);
        
    }

    public  void RestoreUsersFilter() {
        Users.Clear();
        foreach (User user in RestoreUsers) {
            Users.Add(user);
        }
    }

    public  void FilteredUsers(List<User> filteredUsers) {
        Users.Clear();
        foreach (User user in filteredUsers) {
            Users.Add(user);
        }
        ObserverMessage messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"Filtering email");
    }

    public void ExportUsers()
    {
        _fileManager.WriteFile(Users);
    }
    
    public async Task ImportUsers()
    {
        await _fileManager.ReadFile();
        AddExportUsers(_fileManager.GetUsersFromFile());
    }

    private void AddExportUsers(ObservableCollection<User> usersExport)
    {
        foreach (User user in usersExport)
        {
            if (UsersEquals(user))
            {
                Users.Add(user);
                RestoreUsers.Add(user);
            };
        }
    }
    private bool UsersEquals(User user)
    {
        foreach (User userFromDB in Users)
        {
            if (userFromDB.ToString().Equals(user.ToString())) return false;
        }

        return true;
    }
}